package com;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

/**
 * Created by omura on 10/14/2016.
 */
public class Utils {

    private static int red = 0;
    private static int green = 0;
    private static int blue = 0;


    public static boolean isBlue(BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();
        System.out.println("width, height: " + w + ", " + h);

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                System.out.println("x,y: " + j + ", " + i);
                int pixel = image.getRGB(j, i);
                printRGB(pixel);
                System.out.println("");
                return true;
            }
        }
        if(blue>100 && green < 50 && red < 10)
            return true;
        else
            return false;
    }

    public static void printRGB(int pixel) {
        red = (pixel >> 16) & 0xff;
        green = (pixel >> 8) & 0xff;
        blue = (pixel) & 0xff;
        System.out.println("argb: " + red + ", " + green + ", " + blue);
    }
}
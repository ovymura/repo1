import com.google.common.base.Function;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ThreadGuard;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.SystemClock;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by omura on 10/14/2016.
 */
public class TestImages {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestImages.class);

    static WebDriver driver;
    static String driverPath = "src\\test\\resources\\";

    @BeforeClass
    public static void execBeforeClass(){
        System.out.println("Launching IE browser");
        System.setProperty("webdriver.ie.driver", "src\\test\\resources\\IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void execAfterClass(){
        LOG.info("this method executes after class runs");
        driver.close();
        driver.quit();
    }

//    @Test
    public void testPageTitle() {
        driver.navigate().to("http://www.google.com");
        String strPageTitle = driver.getTitle();
        System.out.println("Page title: - "+strPageTitle);
        Assert.assertTrue(strPageTitle.contains("WebDriver"));
    }
}

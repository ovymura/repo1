package pages

import geb.Page

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import com.Utils;

/**
 * Created by omura on 10/14/2016.
 */
class HomePage extends Page {

    static url = "/"
    static at = {
        browser.driver.manage().window().maximize()

        waitFor(20){ googleLogo.displayed == true }
        waitFor(20){ inputText.displayed == true }
    }

    static content = {
        googleSearchButton {$("div.jsb input")}
        googleLogo {$("img#hplogo")}
        inputText {$("input#lst-ib.gsfi")}
        searchBtn {$("span.sbico")}
        result {$("div.srg")}
        blueSkyImage {$("div.rg_ul")}
    }

    public void search(){
        waitFor(20){ inputText.displayed == true }
        waitFor(20){ inputText.value("geb spock")}
        waitFor(20){ searchBtn.click()}
    }

    public void search(String term){
        waitFor(20){ inputText.displayed == true }
        waitFor(20){ inputText.value(term)}
        waitFor(20){ searchBtn.click()}
    }

    public boolean validate(String msg){
        waitFor(20){result.find("h3.r a")[0].displayed}
        return result.find("h3.r a")[0].text().contains(msg)
    }

    public boolean validateBlueSkyImage(){
        boolean isBlue = false
        waitFor(20){blueSkyImage.find("g-img")[0].displayed}
        waitFor(20){blueSkyImage.find("g-img")[0].click()}
        waitFor(20){$("div.irc_t").displayed}
        System.out.println("Blue Sky Image Url: " + $("div.irc_t").find("img").getAttribute("src"))
        try {
            URL url1 = new URL($("div.irc_t").find("img").getAttribute("src"))
            BufferedImage image = ImageIO.read(url1)
            isBlue = Utils.isBlue(image);
        }catch (IOException e){
            e.printStackTrace()
        }
        return isBlue
    }
}
package testSuite

import geb.spock.GebReportingSpec
import geb.spock.GebSpec
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import pages.HomePage
import spock.lang.*;


/**
 * Created by omura on 10/14/2016.
 */
@Stepwise
class SmokeTest extends GebReportingSpec {

    def setupSpec(){}
    def cleanupSpec(){ driver.close() }
    def setup() {}
    def cleanup(){}

    @Test
    def "TC:01 - Load Google Home Page"(){
        expect: "Load Google Page"
        HomePage gPage = browser.to(HomePage)
    }

    @Test
    def "TC:02 - Validate 'geb spock' Search"(){
        given: "Google Homepage"
            HomePage homePage = browser.to(HomePage)
        when: "Perform a search for the 'geb spock' term"
            homePage.search()

        then: "Validate the search result page contain 'Geb - Very Groovy Browser Automation'"
            homePage.validate("Geb - Very Groovy Browser Automation")
    }

    @Test
    def "TC:03 - Search, Download, and Validate the Blue Color for 'blue sky image'"(){
        given: "Google Homepage"
            HomePage page = browser.to(HomePage)
        when: "Search for  'blue sky image' as search term"
            page.search("blue sky image")

        then: "Validate a blue sky image is displayed in result set"
            page.validateBlueSkyImage()
    }
}

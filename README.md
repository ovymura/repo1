# README #

### Blue Sky Image Automation Project ###

* This project is an example of UI Automation using Selenium, Java, Groovy, Spock, Geb, and Gradle
* Version 1

### How do I get set up? ###

* Install the project
* Download the project - imgs_automation 
* Build the project running the follwoing command in windows machine. Command: gradlew build
* Take a look at the reports at the following locations: target\geb-reports, build\reports\tests

  - build.gradle - the file which builds the project, in the root directory
  - GebConfig.groovy - it configures the geb framework, location - test\resources
  - SpockConfig.groovy - it configures the spock framework, location - test\resources

* Dependencies: selenium-api, selenium-java, groovy-all, jcl-over-slf4j, spock-core, geb-spock, selenium-ie-driver


### TEST CASES ###

* TC:01 - Validate Loading the Google Home Page
* TC:02 - Validate 'geb spock' Search Results In Google
* TC:03 - Search, Download, and Validate the Blue Color for 'blue sky image'

### Who do I talk to? ###

* Ovidiu Mura, Email: ovymura@gmail.com